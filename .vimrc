" FILE: ~/.vimrc vim: fdm=marker ts=4

" from vim-scripts package:
packadd! AlignPlugin
packadd! EnhancedCommentify
packadd! gnupg
packadd! supertab
packadd! khuno

" apt install vim-ctrlp vim-python-jedi vim-fugitive
packadd! CtrlP
packadd! python-jedi
packadd! fugitive

" vcsh vim subtree add --prefix .vim/pack/git/opt/signify https://github.com/mhinz/vim-signify master --squash
packadd! signify

set nocompatible
set visualbell
"set cpoptions="A"
filetype plugin indent on
set spelllang=pl,en

if &term =~ '256color'
	"set t_Co=88 " rxvt-unicode
	set t_Co=256 " rxvt-unicode-256color
	set ttymouse=xterm2
	" tmux will send xterm-style keys when its xterm-keys option is on
	execute "set <xUp>=\e[1;*A"
	execute "set <xDown>=\e[1;*B"
	execute "set <xRight>=\e[1;*C"
	execute "set <xLeft>=\e[1;*D"
endif

" Enable true color
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

syntax on
"set synmaxcol=150	" przyśpiesza działanie przy długich liniach
set background=dark
colorscheme jellybeans
let g:jellybeans_use_term_background_color=1
highlight SpellBad cterm=NONE
highlight LineNr ctermbg=235 guibg=#333333
highlight! link TabLine LineNr
highlight Search cterm=NONE ctermfg=NONE ctermbg=blue

" backup:
set nobackup		" nie trzymaj kopii zapasowych, używaj wersji

" search:
set incsearch		" do incremental searching
set hlsearch		" zaznaczanie szukanego tekstu
"set nowrapscan		" kończ szukanie na końcu pliku (nie zaczynaj od początku)
set ignorecase
set smartcase		" zwracaj uwagę na wielkość liter tylko gdy wpisana jakaś wielka litera
set shortmess-=S	" show search count
set splitbelow
set splitright

" history:
set history=50		" keep 50 lines of command line history
set viminfo='20,\"50	" read/write a .viminfo file, don't store more than 50 lines of registers

" status line:
set laststatus=2	" zawsze pokazuj linię statusu
set statusline=%{winnr()}:
set statusline+=%<%F%*				" filename
set statusline+=%=				" left/right separator
set statusline+=%#ErrorMsg#			" errors / warnings start here
set statusline+=%{&ff!='unix'?'['.&ff.']':''}	" fileformat isn't unix
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''} " file encoding isn't UTF-8
if filereadable($HOME.'/.vim/ftplugin/python/khuno.vim')
	set statusline+=%{khuno#Status()}		" flake8 status
endif
set statusline+=%*				" end of errors / warnings
if filereadable($HOME.'/.vim/plugin/fugitive.vim')
	set statusline+=%#User1#			" User1 colors
	set statusline+=%{fugitive#statusline()}	" GIT branch name
	set statusline+=%*				" end of User1
endif
" if syntax debugging
"	set statusline+=\[
"	set statusline+=%{synIDattr(synIDtrans(synID(line('.'),col('.'),1)),'name')}
"	set statusline+=\ 
"	set statusline+=%{synIDattr(synID(line('.'),col('.'),1),'name')}
"	set statusline+=\]
set statusline+=%y				" file type
set statusline+=%([%M%R%H%W]%)			" flags
set statusline+=\ %l:%v\ %p%%			" line, column, position in file

" mode line:
set showcmd		" display incomplete commands
set showmode		" show current mode
set showmatch		" pokaz otwierający nawias gdy wpisze zamykający

" buffers:
set hidden		" nie wymagaj zapisu gdy przechodzisz do nowego bufora
set autochdir		" cd na katalog bufora, cos jak:    autocmd BufEnter * :lcd %:p:h
set path+=**	" add subdirs to the path so that :find can find them
set browsedir=buffer	" To get the File / Open dialog box to default to the current file's directory
set switchbuf=usetab,newtab " opens a tab for every new buffer and switches to the right one

" paths:
set dictionary=/usr/share/dict/words	" used with CTRL-X CTRL-K

" wygląd:
set title
set ruler		" show the cursor position all the time
set number		" wyświetlaj nr linii
set wildmenu		" wyświetlaj linie z menu podczas dopełniania
"set wildmode=longest:full	" dopełniaj jak w BASHu
"set listchars=tab:>-	" pokazuj >----- zamiast tabów
"set list
"set fillchars=vert:║,fold:═,stl:═,stlnc:═
"set fillchars=vert:│,fold:─,stl:─,stlnc:─
set fillchars=
set cursorline			" zaznacz linię z kursorem
set colorcolumn=80
let g:indentLine_color_term = 236
set showtabline=2		" zawsze pokazuj pasek tabów
" terminal
set mouse=a	" myszka też w konsoli
" GUI
set guifont=Ubuntu\ Mono\ derivative\ Powerline\ 10,Andale\ Mono\ 9,Source\ Code\ Pro\ Semi-Bold\ 8,Bitstream\ Vera\ Sans\ Mono\ 9,DejaVu\ Sans\ Mono\ 8
set guioptions=abcegirLt	" m.in: włącz poziomy scrollbar, wyłącz toolbar (T)

if has("gui_running")
	set foldcolumn=2		" szerokość kolumny z zakładkami
	set nowrap
else
	"let g:indentLine_loaded = 1	" disable indentLine plugin in terminal
	"set ts=4		" jak odpalony w konsoli to znaki tabulacji o polowe mniejsze
endif
if &diff
	set wrap
endif

" zachowanie:
behave xterm
"set keymodel=startsel,stopsel	" zaznaczanie z shiftem
set scrolloff=5			" przewijaj juz na 5 linii przed końcem
set backspace=indent,eol,start
set formatoptions=tcrqn		" opcje wklejania (jak maja być tworzone wcięcia itp.)
set confirm			" pytaj przed wyjsciem gdy niezachowane zmiany / plik się zmienił, etc.
set nostartofline		" nie przesuwaj kursowra przy scrollowaniu

" uzupełnianie:
set completeopt=menuone,preview,longest
set omnifunc=syntaxcomplete#Complete " Default to omni completion using the syntax highlighting files
set wildignore+=*.a,*.o,*.pyc,*.pyo
set wildignore+=.git,.hg,.svn
set wildignore+=*~,*.swp,*.tmp

if executable('ag')
	set grepprg=ag\ --nogroup\ --nocolor
endif

" folds:
set foldtext=MojFoldText()	" tekst po zwinięciu zakładki
set foldminlines=3		" minimum 3 linie aby powstał fold
function! MojFoldText()
	let linia = getline(v:foldstart)
	let linia = substitute(linia, '/\*\|\*/\|{{{\d\=\|//\|<!--\|-->', '', 'g')	"}}}
	return v:folddashes.' '.v:foldend.' (ZWINIĘTO: '.(v:foldend-v:foldstart+1).') '.linia
endfunction

" }}}

""""""""""""""""""" AUTO COMMANDS: """"""""""""""""""""""""""""""""""{{{
"autocmd BufNewFile * startinsert	" rozpoczyna w trybie INSERT
autocmd BufRead *.orig set readonly
autocmd BufRead *.mako	setf mako
"autocmd FileType help wincmd L " przesun okno pomocy w prawo

" zaczynaj od ostatniej znanej pozycji kursora:
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | execute "normal g`\"" | endif

" w plikach tekstowych zawijaj tekst po 78. znakach:
"autocmd FileType text setlocal textwidth=78

autocmd FileType python setlocal tags+=$HOME/.vim/tags/python.ctags	" $ ctags -R -f ~/.vim/tags/python.ctags /usr/lib/python2.7/
autocmd FileType python	setlocal expandtab tabstop=4 shiftwidth=4 makeprg=pep8\ --repeat\ --ignore=E501\ %
autocmd FileType python	match ErrorMsg /\s\+$/
autocmd FileType lua	setlocal expandtab tabstop=4 shiftwidth=4

autocmd BufNewFile,Bufread psql\.edit\.* set ft=sql

autocmd BufNewFile,BufRead neomutt-*-\w\+ set ft=mail
" włącz sprawdzanie pisowni w Mutt:
autocmd BufNewFile,BufRead {neo}\\\{0,1\}mutt-*-\w\+ set spell nonu
" wszytko po 80 linii na czerwono
autocmd BufNewFile,BufRead {neo}\\\{0,1\}mutt-*-\w\+ match ErrorMsg /\%>79v.\+/

au BufReadCmd *.epub call zip#Browse(expand("<amatch>"))
"}}}

""""""""""""""""""" KLAWISZOLOGIA: """"""""""""""""""""""""""""""""""{{{
nnoremap Q	<nop>	" nie używam Ex Mode
set pastetoggle=<F11>	" przełączanie w tryb wklejania (nie będzie automatycznych wcięć, ...)
map	<F1>	:vert h<CR>

" zmiana zakładki (taba):
for i in range(1, 9)
	" map	<a-5>	5gt
	execute 'map <a-' . i . '> ' . i . 'gt'
	" the same for terminal (TODO: recognize this key as alt)
	execute 'map ' . i . ' ' . i . 'gt'
endfor
" przemieszczanie zakładek (tabów):
nn	<silent><Esc>j	:if tabpagenr("$") == 1 \| bprevious \| else \| tabprev \| endif<CR>
nn	<silent><Esc>k	:if tabpagenr("$") == 1 \| bnext \| else \| tabnext \| endif<CR>
nn	<silent><Esc>J  :if tabpagenr() == 1\|exe "tabm ".tabpagenr("$")\|el\|exe "tabm ".(tabpagenr()-2)\|en<CR>
nn	<silent><Esc>K  :if tabpagenr() == tabpagenr("$")\|tabm 0\|el\|exe "tabm ".tabpagenr()\|en<CR>           
nn	<silent><M-left>	:if tabpagenr("$") == 1 \| bprevious \| else \| tabprev \| endif<CR>
nn	<silent><M-right>	:if tabpagenr("$") == 1 \| bnext \| else \| tabnext \| endif<CR>
nn	<silent><M-S-left>	:if tabpagenr() == 1\|exe "tabm ".tabpagenr("$")\|el\|exe "tabm ".(tabpagenr()-2)\|en<CR>
nn	<silent><M-S-right>	:if tabpagenr() == tabpagenr("$")\|tabm 0\|el\|exe "tabm ".tabpagenr()\|en<CR>
" zmiana okien
nn	<silent><M-up>		<C-W>W
nn	<silent><M-down>	<C-W>w
for i in range(1, 9)
	" map	<silent><C-W>5		:exe 5 "wincmd w"<CR>
	execute ' map	<silent><C-W>' . i . ' :exe ' . i . '"wincmd w"<CR>'
endfor
" przeskakiwanie między ostatnimi tabami
let g:lasttab = 1
nn	<silent><M-`>		:exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" sprawdzanie pisowni:
"map	<F7>		:w<CR>:!ispell -x -d polish %<CR><CR>:e<CR><CR>
map 	<silent><F7>	:setlocal spell!<CR>
imap	<silent><F7>	<ESC>:setlocal spell!<CR>i<right>

" wyjscie z pliku jak w MC
map 	<silent><F4>	:qall<CR>
imap	<silent><F4>	<ESC>:qall<CR>

" szukaj zaznaczonego tekstu z '*' i '#' (a nie tylko wyrazu pod kursorem):
vnoremap	*	y/<C-R>"<CR>
vnoremap	#	y?<C-R>"<CR>

" wyszukiwanie TYLKO w zaznaczonym fragmencie:
vnoremap	/	<ESC>/\%><C-R>=line("'<")-1<CR>l\%<<C-R>=line("'>")+1<CR>l
vnoremap	?	<ESC>?\%><C-R>=line("'<")-1<CR>l\%<<C-R>=line("'>")+1<CR>l

" nie trać zaznaczenia przy < i >
noremap		<	<gv
noremap		>	>gv

noremap 	<C-j> <C-e>
noremap 	<C-k> <C-y>
inoremap 	<C-j> <C-e>
inoremap 	<C-k> <C-y>

map	<leader><leader>[{V%zf						" \\ wewnatrz bloku {} tworzy fold i go zamyka
map	<Leader>b	GoZ<ESC>:g/^$/.,/./-j<CR>Gdd			" Collapse multiple contiguous empty lines into a single line
map	<Leader>n	GoZ<ESC>:g/^[ <Tab>]*$/.,/[^ <Tab>]/-j<CR>Gdd	" Collapse multiple contiguous blank lines into a single line
map	<Leader>d	:%s/[<Char-128>-<Char-255>]//g			" Delete extended characters (128-255)
map	<Leader>e	:%s/\(.*[^ ][^ ]*\)  *$/\1/c			" Remove trailing spaces at the end of a line

" replace base64 with decoded text
vnoremap <leader>64 y:let @"=system('base64 --decode', @")<cr>gvP
"}}}

""""""""""""""""""" FUNKCJE: """"""""""""""""""""""""""""""""""""""""{{{
function! s:DiffWithSaved()
	let filetype=&ft
	diffthis
	" new | r # | normal 1Gdd - for horizontal split
	vnew | r # | normal 1Gdd
	diffthis
	execute "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
command! Diff call s:DiffWithSaved()

function! Kompiluj()
	silent lmake
	"redraw!
	if len(getloclist(0))
		lopen
	else
		lclose
	endif
endfunction
nnoremap	<silent><F9>	:call Kompiluj()<CR>

if has("gui_running")
	" TIP #1149: Returns either the contents of a fold or spelling suggestions. {{{
	function! BalloonExpr()
		let foldStart = foldclosed(v:beval_lnum)
		let foldEnd   = foldclosedend(v:beval_lnum)
		let lines = []
		" If we're not in a fold...
		if foldStart < 0
			" If 'spell' is on and the word pointed to is incorrectly spelled, the tool tip will contain a few suggestions.
			let lines = spellsuggest(spellbadword(v:beval_text)[ 0 ], 5, 0)
		else
			let numLines = foldEnd - foldStart + 1
			" Up to 31 lines get shown okay; beyond that, only 30 lines are shown with ellipsis in between to indicate too much.
			" The reason why 31 get shown okay is that 30 lines plus one of ellipsis is 31 anyway...
			if (numLines > 31)
				let lines = getline(foldStart, foldStart + 14)
				let lines += [ '-- Snipped ' . (numLines - 30) . ' lines --' ]
				let lines += getline(foldEnd - 14, foldEnd)
			else
				let lines = getline(foldStart, foldEnd)
			endif
		endif
		return join(lines, has("balloon_multiline") ? "\n" : " ")
	endfunction
	set balloonexpr=BalloonExpr()
	set ballooneval
endif " }}}
"}}}

""""""""""""""""""" PLUGINY: """"""""""""""""""""""""""""""""""""""""{{{


" Vimpager: patrz tez:  ~/.vimpagerrc
let vimpager_disable_x11 = 1
let vimpager_passthrough = 0
"let vimpager_passthrough = 1 " text mniejszy niz wysokosc okna od razu na ekran
if exists('g:vimpager_ptree')
	"if vimpager_ptree[1] == 'psql'
	set nowrap
	"if vimpager_ptree[1] == 'python'
	"	set ft=python
	"endif
	nn <left>	zh2
	nn <right>	zl2
	nn <m-left>	zH2
	nn <m-right>	zL2
	" ignore help screen:
	nn h	h
endif
" Khuno:
let g:khuno_builtins="_,apply"
let g:khuno_max_line_length=200
" Signify:
let g:signify_vcs_list = ['git']
let g:signify_sign_change = '~'
let g:signify_sign_change_delete = '!'
" SuperTab:
let g:SuperTabDefaultCompletionType = "context"
" CtrlP:
let g:ctrlp_by_filename = 0	" search by filename rather than full path by default
let g:ctrlp_open_new_file = 't'
map	<C-q>		<Esc>:CtrlPBuffer<CR>
let g:ctrlp_prompt_mappings = {
    \ 'AcceptSelection("e")': ['<c-t>'],
    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
    \ }
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
      \ --ignore .git --ignore .svn --ignore .hg --ignore .DS_Store --ignore "**/*.pyc" -g ""'
let g:ctrlp_show_hidden = 'n'
" Jedi:
let g:jedi#show_call_signatures = 0
" TOhtml:
let html_use_css=1	" domyslnie uzywa CSS zamiast <font>
let use_xhtml=1		" domyslnie tworzy XHTML zamiast HTML
" Enhanced Commentify:
let g:EnhCommentifyTraditionalMode = "No"
" LaTex Suite:
let g:Tex_CompileRule_dvi = 'latex -src-specials -interaction=nonstopmode $*'
" TagList:
nnoremap	<silent><F8>	:Tlist<CR>
map			<leader>l		:TlistToggle<CR>
" Gundo:
nnoremap	<leader>u	:GundoToggle<CR>
" RESTConsole:
let vrc_horizontal_split = 1

let php_sql_query = 1		" podkreślanie składni SQL w PHP
let php_htmlInStrings = 1	" podkreślanie składni HTML w PHP
let python_highlight_all = 1
"let g:netrw_keepdir = 0
"}}}

if filereadable($HOME."/.vim/iabbrev.vim")
	source $HOME/.vim/iabbrev.vim
endif

" użyj MyTabLine jeżeli powerline nie jest dostępny
if !filereadable($HOME."/.vim/plugin/powerline.vim")
	source $HOME/.vim/tabline.vim
endif

" wczytaj ustawienia specyficzne dla danego komputera
if filereadable($HOME."/.vim/vimrc.local")
	source $HOME/.vim/vimrc.local
endif
