function! MyTabLine()
	let s = '' " complete tabline goes here
	" loop through each tab page
	for t in range(1, tabpagenr('$'))
		" set highlight for tab number and &modified
		let s .= '%#TabLineFill#'
		" set the tab page number (for mouse clicks)
		let s .= '%' . t . 'T'
		" set page number string
		let s .= t . ':'
		" get buffer names and statuses
		let n = ''	"temp string for buffer names while we loop and check buftype
		let m = 0	" &modified counter
		let bc = len(tabpagebuflist(t))	"counter to avoid last '|'
		" loop through each buffer in a tab
		let fpaths = []
		for b in tabpagebuflist(t)
			let bc -= 1

			" show the same buffer once, even if it's in multiple windows
			let fpath = bufname(b)
			if index(fpaths, fpath) > -1
				continue
			endif
			call extend(fpaths, [fpath])

			" buffer types: quickfix gets a [Q], help gets [H]{base fname}
			" others get 1dir/2dir/3dir/fname shortened to 1/2/3/fname
			if index(['help', 'quickfix'], getbufvar(b, "&buftype")) > -1
				continue " ignore help/quickfix window names (● sign stays)
			else
				"let n .= pathshorten(fpath)
				let n .= fnamemodify(fpath, ":t")
			endif
			" check and ++ tab's &modified count
			if getbufvar(b, "&modified")
				let m += 1
			endif
			" no final '|' added...formatting looks better done later
			if bc > 0
				let n .= '%#TabLineFill#'
				let n .= '●'
				if t == tabpagenr() " && tabpagewinnr(t) == bufwinnr(b)
					let n .= '%#TabLineSel#'
				else
					let n .= '%#TabLine#'
				endif
			endif
		endfor
		" add modified label [n+] where n pages in tab are modified
		if m > 0
			let s .= '[' . m . '+]'
		endif
		" select the highlighting for the buffer names
		" my default highlighting only underlines the active tab
		" buffer names.
		if t == tabpagenr()
			let s .= '%#TabLineSel#'
		else
			let s .= '%#TabLine#'
		endif
		if n == ''
			let n = '[NO NAME]'
		endif
		" add buffer names
		let s .= n
		" switch to no underlining and add final space to buffer list
		let s .= '%#TabLine#' . ' '
	endfor
	" after the last tab fill with TabLineFill and reset tab page nr
	let s .= '%#TabLineFill#%T'
	" right-align the label to close the current tab page
	if tabpagenr('$') > 1
		let s .= '%=%#TabLineFill#%999XX'
	endif
	return s
endfunction

set tabline=%!MyTabLine()
