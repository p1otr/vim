#! /bin/sh

if [ ! -d ".vim" ]; then echo "no .vim directory"; exit 1; fi

if [ "$1" = "" ]; then
	sudo apt install vim-scripts vim-python-jedi vim-snipmate vim-fugitive vim-ctrlp vim-khuno exuberant-ctags powerline
	#vam install calendar gnupg jinja mako matchit python-indent python-jedi snipmate supertab taglist xmledit powerline

	# tylko na głównym (inicializacja):
	if [ ! -d ".vim/pack/git/opt/signify" ]; then
		vcsh vim remote add -f signify https://github.com/mhinz/vim-signify
		vcsh vim subtree add --prefix .vim/pack/git/opt/signify signify master --squash
	fi
elif [ "$1" = "update" ]; then
	vcsh vim subtree pull --prefix .vim/pack/git/opt/signify signify master --squash
else
	echo "aby uaktualnić subtree: update"
fi
